'use strict';

var babel = require('@babel/core');
// var browserify = require("browserify");
// var babelify = require("babelify");
// var fs = require("fs");

// var b = browserify();

// const fsStreamer = fs.createWriteStream("./bundle.js");

function endsWith(str, search) {
	return str.indexOf(search, str.length - search.length) !== -1;
}

module.exports = ({ types: t }) => {
	return {
		name: 'transform-file-string',
		visitor: {
			ImportDeclaration: {
				exit: (path, state) => {
					let arrPath = state.file.opts.filename.split('/');
					const fileName = arrPath.pop();
					const pathFile = arrPath.join('/');

					const node = path.node;
					const sourceValue = node.source.value;
					if (endsWith(sourceValue, '.web')) {
						let name = sourceValue.split('.');
						name.pop();
						const { code } = babel.transformFileSync(
							pathFile + '/' + name.join('.') + '.bundle.js',
							{
								presets: ['minify']
							}
						);

						path.replaceWith(
							t.variableDeclaration('const', [
								t.variableDeclarator(
									t.identifier(node.specifiers[0].local.name),
									t.stringLiteral(code)
								)
							])
						);
					}
				}
			}

			// CallExpression: {
			//   exit: async (path, state) => {
			//     const callee = path.get("callee");
			//     if (callee.isIdentifier() && callee.equals("name", "require")) {
			//       const moduleArg = path.get("arguments")[0];
			//       const argValue = moduleArg.node.value;
			//       if (
			//         moduleArg &&
			//         moduleArg.isStringLiteral() &&
			//         endsWith(argValue, ".web")
			//       ) {
			//         if (path.parentPath.isVariableDeclarator()) {
			//           let arrPath = state.file.opts.filename.split("/");
			//           arrPath.pop();
			//           const cwd = arrPath.join("/");

			//           const codeStr = await getStr(cwd + "/" + argValue + ".js");
			//           console.log("codeStr2", codeStr);

			//           path.replaceWith(t.stringLiteral(codeStr));
			//         }
			//       }
			//     }
			//   }
			// }
		}
	};
};

// const getStr = async path => {
//   const puck = () => {
//     new Promise(resolve => {
//       b.add(path);
//       b.transform(babelify, { presets: ["@babel/preset-env"] });
//       b.bundle()
//         .pipe(fsStreamer)
//         .on("close", resolve);
//     });
//   };

//   await puck();
//   const p = babel.transformFileSync("./bundle.js", {
//     presets: ["minify"]
//   });

//   return p.code;
// };
