var browserify = require("browserify");
var babelify = require("babelify");
var fs = require("fs");

var b = browserify();

const execFile = require("child_process").execFile;

function endsWith(str, search) {
  return str.indexOf(search, str.length - search.length) !== -1;
}

execFile("find", ["./src"], function(err, stdout, stderr) {
  const file_list = stdout.split("\n");
  file_list.forEach(element => {
    let arrFileSrc = element.split("/");
    const fullFileName = arrFileSrc.pop();
    const cwd = arrFileSrc.join("/");

    let arrFullFileName = fullFileName.split(".");
    const fileName = arrFullFileName[0];
    if (endsWith(fullFileName, ".web.js")) {
      console.log(fileName, cwd);

      const newPath = cwd + "/" + fileName + ".bundle.js";
      const fsStreamer = fs.createWriteStream(newPath);
      b.add(element);
      b.transform(babelify, { presets: ["@babel/preset-env"] });
      b.bundle()
        .pipe(fsStreamer)
        .on("close", () => console.log("done bundle : ", newPath));
    }
  });

  /* now you've got a list with full path file names */
});
